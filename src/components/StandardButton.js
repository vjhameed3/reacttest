import React from "react";
import Button from "@material-ui/core/Button";

export default function StandardButton(props) {
  return (
    <Button variant="outlined" color={props.color} disabled={props.disabled}>
      {props.text}
    </Button>
  );
}
