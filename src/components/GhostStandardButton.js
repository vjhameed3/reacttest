import React from "react";
import Button from "@material-ui/core/Button";

export default function GhostStandardButton(props) {
  return (
    <Button color={props.color} disabled={props.disabled}>
      {props.text}
    </Button>
  );
}
