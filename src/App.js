import React from "react";
import StandardButton from "./components/StandardButton";
import styles from "./components/standard.module.css";
import GhostStandardButton from "./components/GhostStandardButton";

function App() {
  return (
    <div className="App">
      <h3>Ghost Standard Buttons</h3>
      <div>
        <div className={styles.marginVertical}>
          <GhostStandardButton color="primary" text="Accept" />
        </div>
        <div className={styles.marginVertical}>
          <GhostStandardButton color="primary" text="Accept" disabled={true} />
        </div>
        <div className={styles.marginVertical}>
          <GhostStandardButton color="secondary" text="Accept" />
        </div>
        <div className={styles.marginVertical}>
          <GhostStandardButton
            color="secondary"
            text="Accept"
            disabled={true}
          />
        </div>
      </div>
      <h3>Standard Buttons</h3>
      <div>
        <div className={styles.marginVertical}>
          <StandardButton color="primary" text="Accept" />
        </div>
        <div className={styles.marginVertical}>
          <StandardButton color="primary" text="Accept" disabled={true} />
        </div>
      </div>
    </div>
  );
}

export default App;
